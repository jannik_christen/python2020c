#
# Beispiele mit Print
#


print(100+100)
print("100" + "100")

# Text und Zahlen mischen
print("100", 200)

# Zusammenzählen nur nach Umwandlung in einen String mittels str(200) möglich
print("100" + str(200))


print("""Ana sagte: "Gut'n Tag!" """)

